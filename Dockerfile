FROM maven:3.8.4-openjdk-8-slim

# Install base dependencies
RUN apt-get update \
    && apt-get install -y \
        autoconf \
        jq \
        python3-pip \
        curl \
        parallel \
        libxml2-utils \
    && pip3 install yq \
    && rm -rf /var/lib/apt/lists/*

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Install node and npm
RUN curl -sL https://deb.nodesource.com/setup_12.x |  bash - \
    && apt-get update  \
    && apt-get install -y nodejs \
    && npm install -g npm \
    && rm -rf /var/lib/apt/lists/*

# Install apigee dependencies
RUN npm install -g apigeelint \
    openapi2apigee \
    @apidevtools/swagger-cli

# Default to UTF-8 file.encoding
ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    LANGUAGE=C.UTF-8

# Create dirs and users
RUN mkdir -p /opt/apigee/agent/build \
    && sed -i '/[ -z \"PS1\" ] && return/a\\ncase $- in\n*i*) ;;\n*) return;;\nesac' /root/.bashrc \
    && useradd --create-home --shell /bin/bash --uid 1000 pipelines

WORKDIR /opt/apigee/agent/build
ENTRYPOINT ["/bin/bash"]